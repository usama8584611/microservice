const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
  products: [{
    productId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Product' // Reference to Product model in product-service
    },
    quantity: {
      type: Number,
      required: true
    }
  }],
  userId: {
    type: String,
    required: true
  },
  totalAmount: {
    type: Number,
    required: true
  }
}, { timestamps: true });

module.exports = mongoose.model('Order', orderSchema);
