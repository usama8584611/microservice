const express = require('express');
const mongoose = require('mongoose');
const Order = require('./models/order');
const axios = require('axios'); // For making HTTP requests

const app = express();
const port = process.env.PORT || 3001;

mongoose.connect('mongodb://mongo:27017/orders', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
.then(() => console.log('Connected to MongoDB (Checkout Service)'))
.catch(err => console.error('Error connecting to MongoDB (Checkout Service)', err));

app.use(express.json());

app.post('/api/v1/checkout', async (req, res, next) => {
  try {
    const { products, userId } = req.body;

    // 1. Validate product IDs and quantities (assuming product IDs exist)
    const productValidationPromises = products.map(async (product) => {
      const productResponse = await axios.get(`http://product-service:3000/api/v1/products/${product.id}`);
      if (!productResponse.data) {
        throw new Error(`Product with ID ${product.id} not found`);
      }
      // Additional validation for quantity (e.g., stock availability) can be done here
    });

    await Promise.all(productValidationPromises); // Throw any errors from validation

    // 2. Calculate total order amount
    const totalAmount = products.reduce((acc, product) => acc + (product.quantity * product.price), 0);

    // 3. Create a new order in the database
    const newOrder = new Order({
      products,
      userId,
      totalAmount
    });

    await newOrder.save();

    // 4. (Optional) Send confirmation email or notification to user and/or admin

    res.status(201).json({ message: 'Order created successfully', order: newOrder });
  } catch (err) {
    next(err);
  }
});

app.listen(port, () => console.log(`Checkout Service listening on port ${port}`));
