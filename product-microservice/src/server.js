const express = require('express');
const mongoose = require('mongoose');
const productRoutes = require('./routes/productRoutes');

const app = express();
const port = process.env.PORT || 3000;

mongoose.connect('mongodb://mongo:27017/products', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
.then(() => console.log('Connected to MongoDB (Product Service)'))
.catch(err => console.error('Error connecting to MongoDB (Product Service)', err));

app.use(express.json());
app.use('/api/v1/products', productRoutes);

app.listen(port, () => console.log(`Product Service listening on port ${port}`));